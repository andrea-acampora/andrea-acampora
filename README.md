# Andrea Acampora
### MSc in Computer Science And Engineering @ Unibo

![Java](https://img.shields.io/badge/Java-Good-Green)
![Kotlin](https://img.shields.io/badge/Kotlin-Intermediate-blue)
![Python](https://img.shields.io/badge/Python-Intermediate-blue)
![C](https://img.shields.io/badge/C-Intermediate-blue)
![Scala](https://img.shields.io/badge/Scala-Beginner-yellow)
![C++](https://img.shields.io/badge/C++-Beginner-yellow)
![JavaScript](https://img.shields.io/badge/JavaScript-Beginner-yellow)

<img src="https://github-readme-stats.vercel.app/api?username=andrea-acampora&count_private=true&show_icons=true&theme=gruvbox" height=200px width=550px>

#### All time programming languages
<img src="https://wakatime.com/share/@Arop/7b1d5c62-1d9f-4a3a-836c-c29297ecc0b1.svg" height=400px width=650px>


#### Real time coding activity
<img src="https://wakatime.com/share/@Arop/c3fe2869-5ef5-4bc3-8960-99ffe2d5723f.svg?sanitaze=true" height=300px width=650px>

